create table menedzserek
(
man_id numeric(3) primary key,
nev varchar2(25) not null,
ertekeles numeric(2,1) check (ertekeles between 1 and 5),
email varchar2(30) not null,
belepes_datuma datetime not null
);



insert into menedzserek (man_id, nev, ertekeles, email,belepes_datuma) 
values (1, 'Profi P�ter', 4.9,'profip@ingatlan.com',to_datetime('1998.10.14','YYYY.MM.DD'));
insert into menedzserek (man_id, nev, ertekeles, email) 
values (23, 'Kis K�lm�n', 2.4,'kisk@ingatlan.com');
insert into menedzserek (man_id, nev, ertekeles, email) 
values (12, 'Z�ld Zolt�n', 3.6,'zoldz@ingatlan.com');
insert into menedzserek (man_id, nev, ertekeles, email) 
values (67, 'Nagy Nikolett', 4.1,'nagyn@ingatlan.com');
insert into menedzserek (man_id, nev, ertekeles, email) 
values (49, 'Fekete Fanni', 3.7,'feketef@ingatlan.com');








create table ugyfelek
(
berlo_id numeric(3) primary key,
nev varchar2(25) not null,
email varchar2(30) not null
);



insert into ugyfelek values (23,'Munk�csi Zolt�n','munkacsiz@gmail.com');
insert into ugyfelek values (5,'Nagy Alad�r','nagya@gmail.com');
insert into ugyfelek values (42,'Szab� P�l','szabop2@gmail.com');
insert into ugyfelek values (30,'T�th �rp�d','totharp88@gmail.com');
insert into ugyfelek values (26,'Kiss J�nos','kissjani@gmail.com');










create table tipusok
(
tipus_id numeric(2) primary key,
tipus_nev varchar2(20) not null,
lakhato numeric(1) not null check (lakhato in(0,1))
);



insert into tipusok values (1, 'Kertesh�z',1);
insert into tipusok values (2, 'Panel lak�s',1);
insert into tipusok values (3, 'Kiad� szoba',1);
insert into tipusok values (4, 'Gar�zs',0);
insert into tipusok values (5, 'Rakt�r',0);
insert into tipusok values (6, '�zlethelys�g',0);
insert into tipusok values (7, 'Apartman',1);








create table ingatlanok
(
ingatlan_id numeric(4) primary key,
tipus_id numeric(2) references tipusok(tipus_id),
man_id numeric(3) references menedzserek(man_id),
ker numeric(2) not null,
butorozott numeric(1) not null check (butorozott in(0,1)),
berleti_dij numeric(11) not null,
negyzetmeter numeric(4) not null
);



insert into ingatlanok values
(1,2,23,1,1,300000,100);
insert into ingatlanok values
(24,2,1,8,1,250000,80);
insert into ingatlanok values
(32,1,67,19,1,200000,200);
insert into ingatlanok values
(41,5,12,13,0,120000,150);
insert into ingatlanok values
(51,5,1,2,0,70000,50);
insert into ingatlanok values
(65,6,49,7,0,250000,140);
insert into ingatlanok values
(76,2,1,8,1,160000,80);
insert into ingatlanok values
(82,1,23,17,1,220000,180);
insert into ingatlanok values
(91,1,1,5,20,150000,120);


















create table berlesek
(
ber_id numeric(4) primary key,
berlo_id numeric(3) references ugyfelek(berlo_id),
ingatlan_id numeric(4) references ingatlanok(ingatlan_id),
kezdet datetime default sysdatetime,
lejarat datetime not null
);




insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat)
values
(1,5,8,to_datetime('2020.05.21','YYYY.MM.DD'));
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat)
values
(2,23,7,to_datetime('2020.07.10','YYYY.MM.DD'));
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat)
values
(3,42,6,to_datetime('2022.03.11','YYYY.MM.DD'));
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat)
values
(4,30,5,to_datetime('2020.01.10','YYYY.MM.DD'));
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat)
values
(5,26,4,to_datetime('2022.10.5','YYYY.MM.DD'));
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat)
values
(6,30,3,to_datetime('2021.02.02','YYYY.MM.DD'));
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat)
values
(7,5,2,to_datetime('2022.11.12','YYYY.MM.DD'));





