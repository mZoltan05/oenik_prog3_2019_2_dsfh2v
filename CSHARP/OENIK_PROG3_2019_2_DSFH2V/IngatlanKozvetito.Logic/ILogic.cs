﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace IngatlanKozvetito.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using IngatlanKozvetito.Data;

    /// <summary>
    /// This interface does the logical tasks before data send forward to the Repository.
    /// </summary>
    internal interface ILogic
    {
        /// <summary>
        /// This method sends the recently created entity to the create method.
        /// </summary>
        /// <param name="uj">The recently created menedzser entity.</param>
        void CreateMenedzser(menedzserek uj);

        /// <summary>
        /// This method sends the recently created entity to the create method.
        /// </summary>
        /// <param name="uj">The recently created ingatlan entity.</param>
        void CreateIngatlan(ingatlanok uj);

        /// <summary>
        /// This method sends the recently created entity to the create method.
        /// </summary>
        /// <param name="uj">The recently created berles entity.</param>
        void CreateBerles(berlesek uj);

        /// <summary>
        /// This method sends the recently created entity to the create method.
        /// </summary>
        /// <param name="uj">The recently created tipus entity.</param>
        void CreateTipus(tipusok uj);

        /// <summary>
        /// This method sends the recently created entity to the create method.
        /// </summary>
        /// <param name="uj">The recently created ugyfel entity.</param>
        void CreateUgyfel(ugyfelek uj);

        /// <summary>
        /// This method reads all Menedzser entities.
        /// </summary>
        /// <returns>The menedzser collection.</returns>
        IQueryable<menedzserek> ReadMenedzser();

        /// <summary>
        /// This method reads all Ingatlan entities.
        /// </summary>
        /// <returns>The ingatlan collection.</returns>
        IQueryable<ingatlanok> ReadIngatlan();

        /// <summary>
        /// This method reads all Berles entities.
        /// </summary>
        /// <returns>The berles collection.</returns>
        IQueryable<berlesek> ReadBerles();

        /// <summary>
        /// This method reads all Tipus entities.
        /// </summary>
        /// <returns>The tipus collection.</returns>
        IQueryable<tipusok> ReadTipus();

        /// <summary>
        /// This method reads all Ugyfel entities.
        /// </summary>
        /// <returns>The ugyfel collection.</returns>
        IQueryable<ugyfelek> ReadUgyfel();

        /// <summary>
        /// This method updates a menedzser entity.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <param name="attributeName">Attribute of entity to update.</param>
        /// <param name="newValue">The new value.</param>
        void UpdateMenedzser(string id, string attributeName, string newValue);

        /// <summary>
        /// This method updates an ingatlan entity.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <param name="attributeName">Attribute of entity to update.</param>
        /// <param name="newValue">The new value.</param>
        void UpdateIngatlan(string id, string attributeName, string newValue);

        /// <summary>
        /// This method updates a berles entity.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <param name="attributeName">Attribute of entity to update.</param>
        /// <param name="newValue">The new value.</param>
        void UpdateBerles(string id, string attributeName, string newValue);

        /// <summary>
        /// This method updates an ugyfel entity.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <param name="attributeName">Attribute of entity to update.</param>
        /// <param name="newValue">The new value.</param>
        void UpdateUgyfel(string id, string attributeName, string newValue);

        /// <summary>
        /// This method updates a tipus entity.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <param name="attributeName">Attribute of entity to update.</param>
        /// <param name="newValue">The new value.</param>
        void UpdateTipus(string id, string attributeName, string newValue);

        /// <summary>
        /// This method removes a menedzser entity by id.
        /// </summary>
        /// <param name="id">Id of the menedzser to remove.</param>
        void DeleteMenedzser(string id);

        /// <summary>
        /// This method removes an ingatlan entity by id.
        /// </summary>
        /// <param name="id">Id of the ingatlan to remove.</param>
        void DeleteIngatlan(string id);

        /// <summary>
        /// This method removes a berles entity by id.
        /// </summary>
        /// <param name="id">Id of the berles to remove.</param>
        void DeleteBerles(string id);

        /// <summary>
        /// This method removes a tipus entity by id.
        /// </summary>
        /// <param name="id">Id of the tipus to remove.</param>
        void DeleteTipus(string id);

        /// <summary>
        /// This method removes an ugyfel entity by id.
        /// </summary>
        /// <param name="id">Id of the ugyfel to remove.</param>
        void DeleteUgyfel(string id);

        /// <summary>
        /// This method calculate average costs of real estates group by type.
        /// </summary>
        /// <returns>A list of a custom class, that contains the type and average cost.</returns>
        List<IngatlanTipusAtlagar> AtlagIngatlanar();

        /// <summary>
        /// This finds the 3 cheapest real estate in the district.
        /// </summary>
        /// <param name="ker">District.</param>
        /// <returns>List of Ingatlan.</returns>
        List<ingatlanok> HaromLegolcsobbKereses(int ker);

        /// <summary>
        /// This method lists the clients order by rent cost.
        /// </summary>
        /// <returns>List of string.</returns>
        List<string> UgyfelekBerletiSzerint();

        /// <summary>
        /// A java application gives cheaper same type real estates.
        /// </summary>
        /// <param name="tipus_id">Id of the type.</param>
        /// <param name="berleti_dij">Cost of rent.</param>
        /// <returns>List of Ingatlan.</returns>
        List<ingatlanok> AjanlatKeres(int tipus_id, int berleti_dij);
    }
}
