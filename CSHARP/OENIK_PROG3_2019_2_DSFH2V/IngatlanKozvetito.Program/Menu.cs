﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace IngatlanKozvetito.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using IngatlanKozvetito.Data;
    using IngatlanKozvetito.Logic;

    /// <summary>
    /// The main menu with submenus.
    /// </summary>
    internal class Menu
    {
        private BusinessLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        public Menu()
        {
            this.logic = new BusinessLogic();
        }

         /// <summary>
         /// This method implements the main menu.
         /// </summary>
        public void Kezdo()
        {
            string bemenet = string.Empty;
            while (bemenet != "q")
            {
                Console.Clear();
                Console.WriteLine("1 - Uj adat bevitel");
                Console.WriteLine("2 - Adatbázis módosítása"); // majd nézz még rá
                Console.WriteLine("3 - Érték törlése az adatbázisból");
                Console.WriteLine("4 - Listázás");
                Console.WriteLine("5 - Átlagos Ingatlanár Típus szerint");
                Console.WriteLine("6 - A 3 legolcsóbb lakható ingatlan az adott kerületben");
                Console.WriteLine("7 - Ügyfelek listázása bérleti díj szerint");
                Console.WriteLine("8 - A kiválasztott ingatlanra olcsóbbakat kínál - java");
                Console.WriteLine("q - Kilépés");
                bemenet = Console.ReadLine();
                switch (bemenet)
                {
                    case "1": this.UjAdat(); break;
                    case "2": this.Modositas(); break;
                    case "3": this.Torles(); break;
                    case "4": this.ListaValaszto(); break;
                    case "5": this.AtlagIngatlanar(); break;
                    case "6": this.HaromLegolcsobb(); break;
                    case "7": this.UgyfelekBerletiSzerint(); break;
                    case "8": this.AjanlatKeres(); break;
                }
            }
        }

        private void AjanlatKeres()
        {
            Console.Clear();
            Console.WriteLine("Adja meg az ingatlan típusát(1-5):");
            int tipus = int.Parse(Console.ReadLine());
            Console.WriteLine("Adja meg hogy mennyit pénzt szán rá:");
            int ar = int.Parse(Console.ReadLine());
            foreach (ingatlanok item in this.logic.AjanlatKeres(tipus, ar))
            {
                Console.WriteLine(item.berleti_dij + "Ft - " + item.ker + ". kerület" + " - " + item.negyzetmeter + "nm ");
            }

            Console.ReadKey();
        }

        private void UgyfelekBerletiSzerint()
        {
            Console.Clear();
            foreach (string item in this.logic.UgyfelekBerletiSzerint())
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }

        private void HaromLegolcsobb()
        {
            Console.Clear();
            Console.WriteLine("Adja meg a kerületet:");
            foreach (ingatlanok item in this.logic.HaromLegolcsobbKereses(int.Parse(Console.ReadLine())))
            {
                Console.WriteLine(item.tipusok.tipus_nev + " - Ár:" + item.berleti_dij + " - " + item.negyzetmeter + "nm");
            }

            Console.ReadKey();
        }

        private void AtlagIngatlanar()
        {
            Console.Clear();
            foreach (IngatlanTipusAtlagar item in this.logic.AtlagIngatlanar())
            {
                Console.WriteLine(item.Tipus + " - " + item.Atlagar);
            }

            Console.ReadKey();
        }

        private void Torles()
        {
            Console.Clear();
            Console.WriteLine("A tábla kapcsolatok miatt lehetséges, hogy néhány törlés nem fog működni.");
            Console.WriteLine("Melyik táblából szeretne törölni:");
            string bemenet = string.Empty;
            while (bemenet != "q")
            {
                Console.WriteLine("1 - Menedzserek");
                Console.WriteLine("2 - Ingatlanok");
                Console.WriteLine("3 - Bérlések");
                Console.WriteLine("4 - Típusok");
                Console.WriteLine("5 - Ügyfelek");
                bemenet = Console.ReadLine();
                switch (bemenet)
                {
                    case "1": this.MenedzserTorles(); Console.WriteLine("Törölve"); bemenet = "q"; break;
                    case "2": this.IngatlanTorles(); Console.WriteLine("Törölve"); bemenet = "q"; break;
                    case "3": this.BerlesTorles(); Console.WriteLine("Törölve"); bemenet = "q"; break;
                    case "4": this.TipusTorles(); Console.WriteLine("Törölve"); bemenet = "q"; break;
                    case "5": this.UgyfelTorles(); Console.WriteLine("Törölve"); bemenet = "q"; break;
                }
            }
        }

        private void MenedzserTorles()
        {
            Console.Clear();
            Console.WriteLine("Mendzser ID:");
            this.logic.DeleteMenedzser(Console.ReadLine());
        }

        private void IngatlanTorles()
        {
            Console.Clear();
            Console.WriteLine("Ingatlan ID:");
            this.logic.DeleteIngatlan(Console.ReadLine());
        }

        private void BerlesTorles()
        {
            Console.Clear();

            Console.WriteLine("Berles ID:");
            this.logic.DeleteBerles(Console.ReadLine());
        }

        private void TipusTorles()
        {
            Console.Clear();
            Console.WriteLine("Típus ID:");
            this.logic.DeleteTipus(Console.ReadLine());
        }

        private void UgyfelTorles()
        {
            Console.Clear();
            Console.WriteLine("Ügyfél ID:");
            this.logic.DeleteUgyfel(Console.ReadLine());
        }

        private void Modositas()
        {
            string bemenet = string.Empty;
            while (bemenet != "q")
            {
                    Console.Clear();
                    Console.WriteLine("Melyik táblát módosítja:");
                    Console.WriteLine("1 - Menedzserek");
                    Console.WriteLine("2 - Ingatlanok");
                    Console.WriteLine("3 - Bérlések");
                    Console.WriteLine("4 - Típusok");
                    Console.WriteLine("5 - Ügyfelek");
                    Console.WriteLine("q - Főmenü");
                    bemenet = Console.ReadLine();
                    switch (bemenet)
                    {
                        case "1": this.ModositMenedzser(); break;
                        case "2": this.ModositIngatlan(); break;
                        case "3": this.ModositBerles(); break;
                        case "4": this.ModositTipus(); break;
                        case "5": this.ModositUgyfel(); break;
                    }
                }
        }

        private void ModositMenedzser()
        {
            string bemenet = string.Empty;
            string id;
            Console.WriteLine("Adja meg az ID-t:");
            id = Console.ReadLine();
            while (bemenet != "q")
            {
                Console.Clear();

                Console.WriteLine("Mit szeretne módosítani:");

                Console.WriteLine("1 - Név");
                Console.WriteLine("2 - Értékelés");
                Console.WriteLine("3 - Email cím");
                Console.WriteLine("q - Főmenü");
                bemenet = Console.ReadLine();
                Console.WriteLine("Új érték");
                switch (bemenet)
                {
                    case "1": this.logic.UpdateMenedzser(id, "nev", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                    case "2": this.logic.UpdateMenedzser(id, "ertekeles", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                    case "3": this.logic.UpdateMenedzser(id, "email", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                }
            }
        }

        private void ModositIngatlan()
        {
            string bemenet = string.Empty;
            string id;
            Console.WriteLine("Adja meg az ID-t:");
            id = Console.ReadLine();
            while (bemenet != "q")
            {
                Console.Clear();

                Console.WriteLine("Mit szeretne módosítani:");

                Console.WriteLine("1 - Bútorozott");
                Console.WriteLine("2 - Bérleti díj");
                Console.WriteLine("3 - Négyzetméter");
                Console.WriteLine("q - Főmenü");
                bemenet = Console.ReadLine();
                Console.WriteLine("Új érték:");
                switch (bemenet)
                {
                    case "1":
                        this.logic.UpdateIngatlan(id, "butorozott", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                    case "2":
                        this.logic.UpdateIngatlan(id, "berleti_dij", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                    case "3":
                        this.logic.UpdateIngatlan(id, "negyzetmeter", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                }
            }
        }

        private void ModositBerles()
        {
            string bemenet = string.Empty;
            string id;
            Console.WriteLine("Adja meg az ID-t:");
            id = Console.ReadLine();
            while (bemenet != "q")
            {
                Console.Clear();

                Console.WriteLine("Mit szeretne módosítani:");

                Console.WriteLine("1 - Lejárati dátum");
                Console.WriteLine("q - Főmenü");
                bemenet = Console.ReadLine();
                Console.WriteLine("Új érték");
                switch (bemenet)
                {
                    case "1":
                        this.logic.UpdateIngatlan(id, "lejarat", Console.ReadLine());
                        Console.WriteLine("Módosítva..");
                        bemenet = "q";
                        break;
                }
            }
        }

        private void ModositTipus()
        {
            Console.Clear();
            Console.WriteLine("A típus menüben nincsenek módosítható paraméterek.");
            Console.ReadKey();
        }

        private void ModositUgyfel()
        {
            string bemenet = string.Empty;
            string id;
            Console.WriteLine("Adja meg az ID-t:\n(");
            foreach (var item in this.logic.ReadUgyfel())
            {
                Console.Write(item.berlo_id + ",");
            }

            Console.WriteLine(")");
            id = Console.ReadLine();
            while (bemenet != "q")
            {
                Console.Clear();

                Console.WriteLine("Mit szeretne módosítani:");

                Console.WriteLine("1 - Név");
                Console.WriteLine("2 - Email cím");
                Console.WriteLine("q - Főmenü");
                bemenet = Console.ReadLine();
                Console.WriteLine("Új érték:");
                switch (bemenet)
                {
                    case "1":
                        this.logic.UpdateIngatlan(id, "nev", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                    case "2":
                        this.logic.UpdateIngatlan(id, "email", Console.ReadLine());
                        Console.WriteLine("Módosítva.."); bemenet = "q";
                        break;
                }
            }
        }

        private void UjAdat()
        {
            string bemenet = string.Empty;
            while (bemenet != "q")
            {
                Console.Clear();
                Console.WriteLine("1 - Új menedzser");
                Console.WriteLine("2 - Új Ingatlan");
                Console.WriteLine("3 - Új Bérlés");
                Console.WriteLine("4 - Új Típus");
                Console.WriteLine("5 - Új Ügyfél");
                Console.WriteLine("q - Főmenü");
                bemenet = Console.ReadLine();
                switch (bemenet)
                {
                    case "1": this.UjMenedzser(); break;
                    case "2": this.UjIngatlan(); break;
                    case "3": this.UjBerles(); break;
                    case "4": this.UjTipus(); break;
                    case "5": this.UjUgyfel(); break;
                }
            }
        }

        private void UjMenedzser()
        {
            Console.Clear();
            menedzserek uj = new menedzserek();
            Console.WriteLine("Menedzser Id:");
            uj.man_id = int.Parse(Console.ReadLine());

            Console.WriteLine("Menedzser neve:");
            uj.nev = Console.ReadLine();

            uj.ertekeles = 3;
            Console.WriteLine("Menedzser email címe:");
            uj.email = Console.ReadLine();
            uj.belepes_datuma = DateTime.Now;
            this.logic.CreateMenedzser(uj);
        }

        private void UjIngatlan()
        {
            Console.Clear();
            ingatlanok uj = new ingatlanok();
            Console.WriteLine("Ingatlan Id:");
            uj.ingatlan_id = int.Parse(Console.ReadLine());
            Console.WriteLine("Típus Id:\n(");
            foreach (tipusok item in this.logic.ReadTipus())
            {
                Console.Write(item.tipus_id + ",");
            }

            Console.WriteLine(")");

            uj.tipus_id = int.Parse(Console.ReadLine());
            Console.WriteLine("Menedzser Id:\n(");

            foreach (menedzserek item in this.logic.ReadMenedzser())
            {
                Console.Write(item.man_id + ",");
            }

            Console.WriteLine(")");

            uj.man_id = int.Parse(Console.ReadLine());

            Console.WriteLine("Kerület: ");
            uj.ker = int.Parse(Console.ReadLine());

            Console.WriteLine("Bútorozott: (0-1)");
            uj.butorozott = int.Parse(Console.ReadLine());
            Console.WriteLine("Bérleti díj:");
            uj.berleti_dij = int.Parse(Console.ReadLine());
            Console.WriteLine("Négyzetméter:");
            uj.negyzetmeter = int.Parse(Console.ReadLine());
            this.logic.CreateIngatlan(uj);
        }

        private void UjBerles()
        {
            Console.Clear();
            berlesek uj = new berlesek();
            Console.WriteLine("Bérleti szerződés Id:");
            uj.ber_id = int.Parse(Console.ReadLine());
            Console.WriteLine("Ügyfél id:\n(");
            foreach (ugyfelek item in this.logic.ReadUgyfel())
            {
                Console.Write(item.berlo_id + ",");
            }

            Console.WriteLine(")");
            uj.berlo_id = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingatlan id:\n(");
            foreach (ingatlanok item in this.logic.ReadIngatlan())
            {
                Console.Write(item.ingatlan_id + ",");
            }

            Console.WriteLine(")");
            uj.ingatlan_id = int.Parse(Console.ReadLine());
            uj.lejarat = DateTime.Now.AddYears(2);
            this.logic.CreateBerles(uj);
        }

        private void UjTipus()
        {
            Console.ReadKey();
            tipusok uj = new tipusok();
            Console.WriteLine("Típus Id:");
            uj.tipus_id = int.Parse(Console.ReadLine());
            Console.WriteLine("Típus név:");
            uj.tipus_nev = Console.ReadLine();
            Console.WriteLine("Lakható: (0-1)");
            uj.lakhato = int.Parse(Console.ReadLine());
            this.logic.CreateTipus(uj);
        }

        private void UjUgyfel()
        {
            Console.Clear();
            ugyfelek uj = new ugyfelek();
            Console.WriteLine("Ügyfél Id:");
            uj.berlo_id = int.Parse(Console.ReadLine());
            Console.WriteLine("Név:");
            uj.nev = Console.ReadLine();
            Console.WriteLine("Email:");
            uj.email = Console.ReadLine();
            this.logic.CreateUgyfel(uj);
        }

        private void ListaValaszto()
        {
            string bemenet = string.Empty;
            while (bemenet != "q")
            {
                Console.Clear();
                Console.WriteLine("1 - Menedzserek listázása");
                Console.WriteLine("2 - Ingatlanok listázása");
                Console.WriteLine("3 - Bérlések listázása");
                Console.WriteLine("4 - Típusok listázása");
                Console.WriteLine("5 - Ügyfelek listázása");
                Console.WriteLine("q - Kilépés");

                bemenet = Console.ReadLine();
                switch (bemenet)
                {
                    case "1": this.MenedzserekKiir(); break;
                    case "2": this.IngatlanokKiir(); break;
                    case "3": this.BerlesekKiir(); break;
                    case "4": this.TipusokKiir(); break;
                    case "5": this.UgyfelekKiir(); break;
                }

                Console.ReadKey();
            }
        }

        private void MenedzserekKiir()
        {
            Console.Clear();
            foreach (menedzserek menedzser in this.logic.ReadMenedzser())
            {
                Console.WriteLine(menedzser.man_id + " " + menedzser.nev + " - " + menedzser.ertekeles + " - " + menedzser.email + " - " + menedzser.belepes_datuma.ToString("yyyy/MM/dd"));
            }
        }

        private void IngatlanokKiir()
        {
            Console.Clear();
            foreach (ingatlanok ingatlan in this.logic.ReadIngatlan())
            {
                Console.WriteLine(ingatlan.ingatlan_id + " " + ingatlan.tipusok.tipus_nev + " - " + ingatlan.ker + ".kerület - " + ingatlan.negyzetmeter + "nm - " + ingatlan.berleti_dij + "Ft/hó");
            }
        }

        private void BerlesekKiir()
        {
            Console.Clear();
            foreach (berlesek berles in this.logic.ReadBerles())
            {
                Console.WriteLine(berles.ber_id + " " + "Bérlő: " + berles.ugyfelek.nev + " - " + berles.ingatlanok.tipusok.tipus_nev + " - Ár:" + berles.ingatlanok.berleti_dij + " - Lejár:" + berles.lejarat.ToString("yyyy/MM/dd"));
            }
        }

        private void TipusokKiir()
        {
            Console.Clear();
            foreach (tipusok tipus in this.logic.ReadTipus())
            {
                Console.Write(tipus.tipus_nev + " - Lakható:");
                if (tipus.lakhato == 1)
                {
                    Console.WriteLine("Igen");
                }
                else
                {
                    Console.WriteLine("Nem");
                }
            }
        }

        private void UgyfelekKiir()
        {
            Console.Clear();
            foreach (ugyfelek ugyfel in this.logic.ReadUgyfel())
            {
                Console.WriteLine(ugyfel.berlo_id + " " + ugyfel.nev + " - " + ugyfel.email);
            }
        }
    }
}
