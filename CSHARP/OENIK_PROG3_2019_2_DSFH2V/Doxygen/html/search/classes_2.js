var searchData=
[
  ['ingatlandbentities_81',['IngatlanDBEntities',['../class_ingatlan_kozvetito_1_1_data_1_1_ingatlan_d_b_entities.html',1,'IngatlanKozvetito::Data']]],
  ['ingatlanok_82',['ingatlanok',['../class_ingatlan_kozvetito_1_1_data_1_1ingatlanok.html',1,'IngatlanKozvetito::Data']]],
  ['ingatlantipusatlagar_83',['IngatlanTipusAtlagar',['../class_ingatlan_kozvetito_1_1_logic_1_1_ingatlan_tipus_atlagar.html',1,'IngatlanKozvetito::Logic']]],
  ['irepository_84',['IRepository',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3aberlesek_20_3e_85',['IRepository&lt; IngatlanKozvetito::Data::berlesek &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3aingatlanok_20_3e_86',['IRepository&lt; IngatlanKozvetito::Data::ingatlanok &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3amenedzserek_20_3e_87',['IRepository&lt; IngatlanKozvetito::Data::menedzserek &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3atipusok_20_3e_88',['IRepository&lt; IngatlanKozvetito::Data::tipusok &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3augyfelek_20_3e_89',['IRepository&lt; IngatlanKozvetito::Data::ugyfelek &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]]
];
