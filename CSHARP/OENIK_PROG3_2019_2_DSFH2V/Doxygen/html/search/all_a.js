var searchData=
[
  ['test_47',['Test',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html',1,'IngatlanKozvetito::Logic::Tests']]],
  ['testcreateberles_48',['TestCreateBerles',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#aeecf41ab2410431e084c6cd0b1b29fc4',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testcreateingatlan_49',['TestCreateIngatlan',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a8d23f185fe293bee3e803252ef166ef7',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testcreatemenedzser_50',['TestCreateMenedzser',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#aadcf184ce92fe9fdcd4f74552f9744ae',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testcreatetipus_51',['TestCreateTipus',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a2bf5a535081889a93f7eb650f3f198bc',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testcreateugyfel_52',['TestCreateUgyfel',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#ae37524927bebed3a50700c28600202f3',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testdeleteberles_53',['TestDeleteBerles',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a0167b90ab2f99d813cdc8ae8b0f4c65e',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testdeleteingatlan_54',['TestDeleteIngatlan',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#af16b7d9fc11821fb6bf3f7a4be376b4e',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testdeletemenedzser_55',['TestDeleteMenedzser',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a04c00eb46a18c65fe906017ce68b8267',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testdeletetipus_56',['TestDeleteTipus',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a2fe6b4c1069b01f1dc0a2e41673e2ccd',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testdeleteugyfel_57',['TestDeleteUgyfel',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#ac9ac8cfd739b0aabe6c97e5fc3f1c598',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testreadberles_58',['TestReadBerles',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a740c4f1443c8249e82ca1db1c966502f',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testreadingatlan_59',['TestReadIngatlan',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a56848a90c6de67d009cef733b009e9c8',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testreadmenedzser_60',['TestReadMenedzser',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#acf539e47936fd4cc5df95930b6bc89b4',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testreadtipus_61',['TestReadTipus',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a27af1e0222cb1d34129fcce3c6382de4',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testreadugyfel_62',['TestReadUgyfel',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a911f614d97d303556bddc33feedab5d2',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testupdateberles_63',['TestUpdateBerles',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#aad6a69fe9cdfd5dbd75db33f6d5dfa85',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testupdateingatlan_64',['TestUpdateIngatlan',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a08a703d25013ec2241acc4ab30572554',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testupdatemenedzser_65',['TestUpdateMenedzser',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a3fe6ba6471fa62736c651e5a8e77aa3c',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testupdatetipus_66',['TestUpdateTipus',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a227cb9b0281c0d1c4108879069140cb6',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['testupdateugyfel_67',['TestUpdateUgyfel',['../class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a1519ba0be9db5380631e0c00811a9942',1,'IngatlanKozvetito::Logic::Tests::Test']]],
  ['tipus_68',['Tipus',['../class_ingatlan_kozvetito_1_1_logic_1_1_ingatlan_tipus_atlagar.html#ad00ec89c71d35562d1c29c84d5753539',1,'IngatlanKozvetito::Logic::IngatlanTipusAtlagar']]],
  ['tipusok_69',['tipusok',['../class_ingatlan_kozvetito_1_1_data_1_1tipusok.html',1,'IngatlanKozvetito::Data']]]
];
