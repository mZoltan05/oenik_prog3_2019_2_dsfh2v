var namespaces_dup =
[
    [ "NUnit 3.11 - October 6, 2018", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md60", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md59", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md61", null ]
    ] ],
    [ "NUnit 3.10.1 - March 12, 2018", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md62", null ],
    [ "NUnit 3.10 - March 12, 2018", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md63", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md64", null ]
    ] ],
    [ "NUnit 3.9 - November 10, 2017", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md65", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md66", null ]
    ] ],
    [ "NUnit 3.8.1 - August 28, 2017", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md67", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md68", null ]
    ] ],
    [ "NUnit 3.8 - August 27, 2017", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md69", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md70", null ]
    ] ],
    [ "NUnit 3.7.1 - June 6, 2017", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md71", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md72", null ]
    ] ],
    [ "NUnit 3.7 - May 29, 2017", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md73", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md74", null ]
    ] ],
    [ "NUnit 3.6.1 - February 26, 2017", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md75", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md76", null ]
    ] ],
    [ "NUnit 3.6 - January 9, 2017", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md77", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md78", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md79", null ]
    ] ],
    [ "NUnit 3.5 - October 3, 2016", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md80", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md81", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md82", null ]
    ] ],
    [ "NUnit 3.4.1 - June 30, 2016", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md83", [
      [ "Console Runner", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md84", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md85", null ]
    ] ],
    [ "NUnit 3.4 - June 25, 2016", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md86", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md87", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md88", null ],
      [ "Console Runner", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md89", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md90", null ]
    ] ],
    [ "NUnit 3.2.1 - April 19, 2016", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md91", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md92", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md93", null ],
      [ "Console Runner", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md94", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md95", null ]
    ] ],
    [ "NUnit 3.2 - March 5, 2016", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md96", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md97", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md98", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md99", null ]
    ] ],
    [ "NUnit 3.0.1 - December 1, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md100", [
      [ "Console Runner", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md101", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md102", null ]
    ] ],
    [ "NUnit 3.0.0 Final Release - November 15, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md103", [
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md104", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 3 - November 13, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md105", [
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md106", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md107", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 2 - November 8, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md108", [
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md109", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md110", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate - November 1, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md111", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md112", null ],
      [ "NUnitLite", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md113", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md114", null ],
      [ "Console Runner", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md115", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md116", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 5 - October 16, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md117", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md118", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md119", null ],
      [ "Console Runner", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md120", [
        [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md121", null ]
      ] ]
    ] ],
    [ "NUnit 3.0.0 Beta 4 - August 25, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md122", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md123", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md124", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md125", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 3 - July 15, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md126", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md127", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md128", null ],
      [ "Console", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md129", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md130", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 2 - May 12, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md131", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md132", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md133", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md134", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 1 - March 25, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md135", [
      [ "General", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md136", null ],
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md137", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md138", null ],
      [ "Console", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md139", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md140", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 5 - January 30, 2015", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md141", [
      [ "General", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md142", null ],
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md143", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md144", null ],
      [ "Console", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md145", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md146", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 4 - December 30, 2014", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md147", [
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md148", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md149", null ],
      [ "Console", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md150", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md151", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 3 - November 29, 2014", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md152", [
      [ "Breaking Changes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md153", null ],
      [ "Framework", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md154", null ],
      [ "Engine", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md155", null ],
      [ "Console", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md156", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md157", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 2 - November 2, 2014", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md158", [
      [ "Breaking Changes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md159", null ],
      [ "General", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md160", null ],
      [ "New Features", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md161", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md162", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 1 - September 22, 2014", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md163", [
      [ "Breaking Changes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md164", null ],
      [ "General", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md165", null ],
      [ "New Features", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md166", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md167", null ],
      [ "Console Issues Resolved (Old nunit-console project, now combined with nunit)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md168", null ]
    ] ],
    [ "NUnit 2.9.7 - August 8, 2014", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md169", [
      [ "Breaking Changes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md170", null ],
      [ "New Features", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md171", null ],
      [ "Issues Resolved", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md172", null ]
    ] ],
    [ "NUnit 2.9.6 - October 4, 2013", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md173", [
      [ "Main Features", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md174", null ],
      [ "Bug Fixes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md175", null ],
      [ "Bug Fixes in 2.9.6 But Not Listed Here in the Release", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md176", null ]
    ] ],
    [ "NUnit 2.9.5 - July 30, 2010", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md177", [
      [ "Bug Fixes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md178", null ]
    ] ],
    [ "NUnit 2.9.4 - May 4, 2010", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md179", [
      [ "Bug Fixes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md180", null ]
    ] ],
    [ "NUnit 2.9.3 - October 26, 2009", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md181", [
      [ "Main Features", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md182", null ],
      [ "Bug Fixes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md183", null ]
    ] ],
    [ "NUnit 2.9.2 - September 19, 2009", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md184", [
      [ "Main Features", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md185", null ],
      [ "Bug Fixes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md186", null ]
    ] ],
    [ "NUnit 2.9.1 - August 27, 2009", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md187", [
      [ "General", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md188", null ],
      [ "Bug Fixes", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md189", null ]
    ] ],
    [ "IngatlanKozvetito", "namespace_ingatlan_kozvetito.html", "namespace_ingatlan_kozvetito" ]
];