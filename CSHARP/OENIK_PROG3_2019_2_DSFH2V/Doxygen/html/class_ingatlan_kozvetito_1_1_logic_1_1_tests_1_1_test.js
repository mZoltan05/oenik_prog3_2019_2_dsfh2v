var class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test =
[
    [ "SetUpTests", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#ab4534d3295f81f8eb6f6f3b5c6ecfc3c", null ],
    [ "TestCreateBerles", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#aeecf41ab2410431e084c6cd0b1b29fc4", null ],
    [ "TestCreateIngatlan", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a8d23f185fe293bee3e803252ef166ef7", null ],
    [ "TestCreateMenedzser", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#aadcf184ce92fe9fdcd4f74552f9744ae", null ],
    [ "TestCreateTipus", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a2bf5a535081889a93f7eb650f3f198bc", null ],
    [ "TestCreateUgyfel", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#ae37524927bebed3a50700c28600202f3", null ],
    [ "TestDeleteBerles", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a0167b90ab2f99d813cdc8ae8b0f4c65e", null ],
    [ "TestDeleteIngatlan", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#af16b7d9fc11821fb6bf3f7a4be376b4e", null ],
    [ "TestDeleteMenedzser", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a04c00eb46a18c65fe906017ce68b8267", null ],
    [ "TestDeleteTipus", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a2fe6b4c1069b01f1dc0a2e41673e2ccd", null ],
    [ "TestDeleteUgyfel", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#ac9ac8cfd739b0aabe6c97e5fc3f1c598", null ],
    [ "TestReadBerles", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a740c4f1443c8249e82ca1db1c966502f", null ],
    [ "TestReadIngatlan", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a56848a90c6de67d009cef733b009e9c8", null ],
    [ "TestReadMenedzser", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#acf539e47936fd4cc5df95930b6bc89b4", null ],
    [ "TestReadTipus", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a27af1e0222cb1d34129fcce3c6382de4", null ],
    [ "TestReadUgyfel", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a911f614d97d303556bddc33feedab5d2", null ],
    [ "TestUpdateBerles", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#aad6a69fe9cdfd5dbd75db33f6d5dfa85", null ],
    [ "TestUpdateIngatlan", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a08a703d25013ec2241acc4ab30572554", null ],
    [ "TestUpdateMenedzser", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a3fe6ba6471fa62736c651e5a8e77aa3c", null ],
    [ "TestUpdateTipus", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a227cb9b0281c0d1c4108879069140cb6", null ],
    [ "TestUpdateUgyfel", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html#a1519ba0be9db5380631e0c00811a9942", null ]
];