/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "IngatlanKozvetito", "index.html", [
    [ "Castle Core Changelog", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html", [
      [ "4.4.0 (2019-04-05)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md1", null ],
      [ "4.3.1 (2018-06-21)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md2", null ],
      [ "4.3.0 (2018-06-07)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md3", null ],
      [ "4.2.1 (2017-10-11)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md4", null ],
      [ "4.2.0 (2017-09-28)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md5", null ],
      [ "4.1.1 (2017-07-12)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md6", null ],
      [ "4.1.0 (2017-06-11)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md7", null ],
      [ "4.0.0 (2017-01-25)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md8", null ],
      [ "4.0.0-beta002 (2016-10-28)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md9", null ],
      [ "4.0.0-beta001 (2016-07-17)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md10", null ],
      [ "4.0.0-alpha001 (2016-04-07)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md11", null ],
      [ "3.3.3 (2014-11-06)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md12", null ],
      [ "3.3.2 (2014-11-03)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md13", null ],
      [ "3.3.1 (2014-09-10)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md14", null ],
      [ "3.3.0 (2014-04-27)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md15", null ],
      [ "3.2.2 (2013-11-30)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md16", null ],
      [ "3.2.1 (2013-10-05)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md17", null ],
      [ "3.2.0 (2013-02-16)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md18", null ],
      [ "3.1.0 (2012-08-05)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md19", null ],
      [ "3.1.0 RC (2012-07-08)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md20", null ],
      [ "3.0.0 (2011-12-13)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md21", null ],
      [ "3.0.0 RC 1 (2011-11-20)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md22", null ],
      [ "3.0.0 beta 1 (2011-08-14)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md23", null ],
      [ "2.5.2 (2010-11-15)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md24", null ],
      [ "2.5.1 (2010-09-21)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md25", null ],
      [ "2.5.0 (2010-08-21)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md26", null ],
      [ "1.2.0 (2010-01-11)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md27", null ],
      [ "1.2.0 beta (2009-12-04)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md28", null ],
      [ "1.1.0 (2009-05-04)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md29", null ],
      [ "Release Candidate 3", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md30", null ],
      [ "0.0.1.0", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019164a0c4a4018ff4c942aa4322942fadb.html#autotoc_md31", null ]
    ] ],
    [ "Analyzer Configuration", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html", [
      [ ".editorconfig format", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md33", null ],
      [ "Enabling .editorconfig based configuration for a project", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md34", null ],
      [ "Supported .editorconfig options", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md35", [
        [ "Analyzed API surface", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md36", null ],
        [ "Analyzed output kinds", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md37", null ],
        [ "Required modifiers for analyzed APIs", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md38", null ],
        [ "Async void methods", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md39", null ],
        [ "Single letter type parameters", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md40", null ],
        [ "Exclude extension method 'this' parameter", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md41", null ],
        [ "Null check validation methods", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md42", null ],
        [ "Additional string formatting methods", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md43", null ],
        [ "Excluded symbol names", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md44", null ],
        [ "Excluded type names with derived types", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md45", null ],
        [ "Unsafe DllImportSearchPath bits when using DefaultDllImportSearchPaths attribute", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md46", null ],
        [ "Exclude ASP.NET Core MVC ControllerBase when considering CSRF", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md47", null ],
        [ "Disallowed symbol names", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md48", null ],
        [ "Dataflow analysis", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md49", [
          [ "Interprocedural analysis Kind", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md50", null ],
          [ "Maximum method call chain length to analyze for interprocedural dataflow analysis", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md51", null ],
          [ "Maximum lambda or local function call chain length to analyze for interprocedural dataflow analysis", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md52", null ],
          [ "Dispose analysis kind for IDisposable rules", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md53", null ],
          [ "Configure dispose ownership transfer for arguments passed to constructor invocation", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md54", null ],
          [ "Configure dispose ownership transfer for disposable objects passed as arguments to method calls", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md55", null ],
          [ "Configure execution of Copy analysis (tracks value and reference copies)", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md56", null ],
          [ "Configure sufficient IterationCount when using weak KDF algorithm", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019ea26947280533b7952912d266c76e8ca.html#autotoc_md57", null ]
        ] ]
      ] ]
    ] ],
    [ "Microsoft", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019c9408b615635661e243ae0e8db1acdda.html", null ],
    [ "NUnit 3.12 - May 14, 2019", "md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_business_logic_8cs_source.html",
"md__c_1__users_admin__documents_oenik_prog3_2019_2_dsfh2v__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20194ceb63946090cb016c9c344508252a5d.html#autotoc_md108"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';