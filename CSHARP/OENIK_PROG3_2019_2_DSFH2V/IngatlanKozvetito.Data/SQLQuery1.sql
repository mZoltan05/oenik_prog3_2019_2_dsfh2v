﻿create table menedzserek
(
man_id numeric(3) primary key,
nev varchar(25) not null,
ertekeles numeric(2,1) check (ertekeles between 1 and 5),
email varchar(30) not null,
belepes_datuma datetime not null
);

create table ugyfelek
(
berlo_id numeric(3) primary key,
nev varchar(25) not null,
email varchar(30) not null
);

create table tipusok
(
tipus_id numeric(2) primary key,
tipus_nev varchar(20) not null,
lakhato numeric(1) not null check (lakhato in(0,1))
);

create table ingatlanok
(
ingatlan_id numeric(4) primary key,
tipus_id numeric(2) references tipusok(tipus_id),
man_id numeric(3) references menedzserek(man_id),
ker numeric(2) not null,
butorozott numeric(1) not null check (butorozott in(0,1)),
berleti_dij numeric(11) not null,
negyzetmeter numeric(4) not null
);

create table berlesek
(
ber_id numeric(4) primary key,
berlo_id numeric(3) references ugyfelek(berlo_id),
ingatlan_id numeric(4) references ingatlanok(ingatlan_id),
lejarat datetime not null
);

insert into menedzserek (man_id, nev, ertekeles, email,belepes_datuma) 
values (1, 'Profi Péter', 4.9,'profip@ingatlan.com','14-OCT-1998');
insert into menedzserek (man_id, nev, ertekeles, email,belepes_datuma) 
values (23, 'Kis Kálmán', 2.4,'kisk@ingatlan.com','10-DEC-2000');
insert into menedzserek (man_id, nev, ertekeles, email,belepes_datuma) 
values (12, 'Zöld Zoltán', 3.6,'zoldz@ingatlan.com','05-JUN-2012');
insert into menedzserek (man_id, nev, ertekeles, email,belepes_datuma) 
values (67, 'Nagy Nikolett', 4.1,'nagyn@ingatlan.com','23-NOV-2018');
insert into menedzserek (man_id, nev, ertekeles, email,belepes_datuma) 
values (49, 'Fekete Fanni', 3.7,'feketef@ingatlan.com','01-JAN-2015');


insert into ugyfelek values (23,'Munkácsi Zoltán','munkacsiz@gmail.com');
insert into ugyfelek values (5,'Nagy Aladár','nagya@gmail.com');
insert into ugyfelek values (42,'Szabó Pál','szabop2@gmail.com');
insert into ugyfelek values (30,'Tóth Árpád','totharp88@gmail.com');
insert into ugyfelek values (26,'Kiss János','kissjani@gmail.com');


insert into tipusok values (1, 'Kertesház',1);
insert into tipusok values (2, 'Panel lakás',1);
insert into tipusok values (3, 'Kiadó szoba',1);
insert into tipusok values (4, 'Garázs',0);
insert into tipusok values (5, 'Raktár',0);
insert into tipusok values (6, 'Üzlethelység',0);
insert into tipusok values (7, 'Apartman',1);

insert into ingatlanok values (1,2,23,1,1,300000,100);
insert into ingatlanok values (24,2,1,8,1,250000,80);
insert into ingatlanok values (32,1,67,19,1,200000,200);
insert into ingatlanok values (41,5,12,13,0,120000,150);
insert into ingatlanok values (51,5,1,2,0,70000,50);
insert into ingatlanok values (65,6,49,7,0,250000,140);
insert into ingatlanok values (76,2,1,8,1,160000,80);
insert into ingatlanok values (82,1,23,17,1,220000,180);
insert into ingatlanok values (91,1,1,5,20,150000,120);


insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat) values (1,5,24,'21-APR-2020');
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat) values (2,23,32, '10-JUL-2020');
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat) values (3,42,41,'11-MAY-2022');
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat) values (4,30,51,'10-JAN-2020');
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat) values (5,26,65,'05-OCT-2022');
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat) values (6,30,76,'02-FEB-2021');
insert into berlesek (ber_id,berlo_id,ingatlan_id,lejarat) values (7,5,82,'12-NOV-2022');




