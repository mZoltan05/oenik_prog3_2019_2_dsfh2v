/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ingatlan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Filter {
    boolean filterTipus;
    int tipus;
    boolean filterBerleti_dj;
    int berleti_dj;
    
    
    public Filter setFilterTipus(int tipus){
        filterTipus = true;
        this.tipus = tipus;
        return this;
    }
    
    public Filter setFilterBerleti_dj(int berleti_dj){
        filterBerleti_dj = true;
        this.berleti_dj = berleti_dj;
        return this;
    }

     public List<Ingatlan> Filter(List<Ingatlan> ingatlanok){
        List<Ingatlan> filter = new ArrayList<>();
        
        for (Ingatlan ing : ingatlanok) {
            if ((!filterTipus || this.tipus == ing.getTipus_id()) 
                     && (!filterBerleti_dj || ing.getBerleti_dij() < this.berleti_dj)
                    ) {
                
                filter.add(ing);
            }
        }
        return filter;
    }
       
}
