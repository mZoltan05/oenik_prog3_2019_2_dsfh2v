/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ingatlan;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class Repository {
    
    private static Repository instance;

    public static Repository getInstance() {
        if (instance == null) {
            instance = new Repository();
        }
        return instance;
    }
    
    
    List<Ingatlan> ingatlanok;

    
    public List<Ingatlan> getIngatlanok() {
        return ingatlanok;
    }

    
    public Repository() {
        Ingatlan ing1 = new Ingatlan(3,1,50000,30,1); // kerület, butorozott, berleti_dij, negyzetmeter, tipus_id
        Ingatlan ing2 = new Ingatlan(6,1,130000,60,2);
        Ingatlan ing3 = new Ingatlan(5,0,120000,120,1);
        Ingatlan ing4 = new Ingatlan(7,1,160000,200,1);
        Ingatlan ing5 = new Ingatlan(2,1,90000,30,2);
        Ingatlan ing6 = new Ingatlan(10,0,85000,110,1);
        Ingatlan ing7 = new Ingatlan(5,1,60000,43,2);
        Ingatlan ing8 = new Ingatlan(13,1,100000,80,1);
        Ingatlan ing9 = new Ingatlan(6,0,110000,60,5);
        Ingatlan ing10 = new Ingatlan(9,1,130000,130,1);
        
        ingatlanok = asList(ing1,ing2,ing3,ing4,ing5,ing6,ing7,ing8,ing9,ing10);
     }
    
}
    
    

